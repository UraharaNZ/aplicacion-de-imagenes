export const getGifts = async (category) => {
    const offset = Math.floor(Math.random() * (24 - 1 + 1)) + 1;
    const url = `https://api.giphy.com/v1/gifs/search?api_key=S0b5QKRRCAYCTs69F68zxa2Dti8o9d36&q=${category}&limit=24&offset=${offset}`;
    const response = await fetch(url)
    const {data} = await response.json();

    const gifts = data.map(image => {
        if (image.username.length === 0) {
            image.username = 'Giphy';
        }

        return {
            id: image.id,
            title: image.title,
            url: image.images.downsized_medium.url,
            username: image.username
        }
    });

    return gifts;
}