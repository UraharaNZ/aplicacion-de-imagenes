import React from 'react';
import ReactDOM from 'react-dom/client';

import {ImagesApp} from './ImagesApp';

import './tabler.min.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ImagesApp />
  </React.StrictMode>
)
