import {useState} from 'react';
import {AddCategory, GifGrid} from "./components";
import {Nav} from "./components/Nav";
import {Overview} from "./components/Overview";
import {Footer} from "./components/Footer";

export const ImagesApp = () => {

    const [search, setSearch] = useState([]);

    const onAddCategory = (newCategory) => {
        setSearch(
            {
                id: generateString(2),
                category: newCategory,
            }
        );
    }

    return (
        <>
            <Nav/>
            <Overview pretitle={"Projects"} title={"Gifts"}/>

            <div className={"page-body container-xl"}>
                <div className={"row row-cards"}>
                    <div className={"col-12 card"}>
                        <div className={"card-body"}>
                            <AddCategory
                                onNewCategory={onAddCategory}
                            />

                            <GifGrid key={generateString(10)} search={search}/>
                        </div>

                    </div>
                </div>
            </div>

            <Footer />
        </>
    )

    function generateString(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

}