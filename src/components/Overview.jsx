export const Overview = ({pretitle, title}) => {

    return (
        <div className="page-wrapper">
            <div className="container-xl">
                <div className="page-header d-print-none">
                    <div className="row g-2 align-items-center">
                        <div className="col">
                            <div className="page-pretitle">
                                {pretitle}
                            </div>
                            <h2 className="page-title">
                                {title}
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


