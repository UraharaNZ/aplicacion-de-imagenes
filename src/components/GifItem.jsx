import PropTypes from 'prop-types';

export const GifItem = ({title, url, username}) => {

    return (
        <div className="col">
            <div className="card card-sm">
                <a href="#" className="d-block">
                    <img src={url} alt={title} />
                </a>
                <div className="card-body">
                    <div className="align-items-center">
                        <div className="text-center">
                            <p>{title}</p>
                            <div className="text-muted">{username}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}

GifItem.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired
}