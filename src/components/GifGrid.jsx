import {GifItem} from './GifItem';
import {useFetchGifts} from "../hooks/useFetchGifts";

import PropTypes from 'prop-types';
import {AddCategory} from "./AddCategory";

export const GifGrid = ({search}) => {

    const {images, isLoading} = useFetchGifts(search.category);

    return (
        <>
            <h3>{search.category}</h3>
            {
                isLoading && (<h2>Loading...</h2>)
            }


            <div className="row row-cols-6 g-3">
                {
                    images.map((image) => (
                        <GifItem
                            key={image.id}
                            {...image}
                        />
                    ))
                }
            </div>
        </>
    )

}

GifGrid.propTypes = {
    search: PropTypes.isRequired,
}