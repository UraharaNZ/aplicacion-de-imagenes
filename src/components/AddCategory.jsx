import {useState} from 'react';
import {GifItem} from "./GifItem";

import PropTypes from 'prop-types';

export const AddCategory = ({onNewCategory}) => {

    const [inputValue, setInputValue] = useState();

    const onInputChange = ({target}) => {
        setInputValue(target.value);
    }

    const onSubmit = (event) => {
        event.preventDefault();

        if (inputValue === undefined) return;
        if (inputValue.trim().length <= 3) return;

        onNewCategory(inputValue.trim());
    }

    return (
        <form aria-label={"form"} className={"row"} onSubmit={(event) => onSubmit(event)}>
            <div className="input-group mb-2">
                <input type="text" className="form-control" placeholder="Search for images" onChange={(event) => onInputChange(event)}/>
                <button className="btn" type="submit">Search</button>
            </div>
        </form>
    )
}

AddCategory.propTypes = {
    onNewCategory: PropTypes.isRequired,
}