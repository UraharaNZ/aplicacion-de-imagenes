export const Footer = () => {

    return (
        <footer className="footer footer-transparent d-print-none">
            <div className="container-xl">
                <div className="row text-center align-items-center flex-row-reverse">
                    <div className="col-4">
                        <ul className="list-inline list-inline-dots mb-0">
                            <li className="list-inline-item">
                                <a href="https://github.com/sponsors/codecalm" target="_blank"
                                   className="link-secondary" rel="noopener">
                                    Tabler &nbsp;
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         className="icon text-pink icon-filled icon-inline" width="24" height="24"
                                         viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none"
                                         strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path
                                            d="M19.5 12.572l-7.5 7.428l-7.5 -7.428m0 0a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572"></path>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-4">
                        <ul className="list-inline list-inline-dots mb-0">
                            <li className="list-inline-item">
                                <a href="https://www.twitter.com/UraharaNZ" className="link-secondary">UraharaNZ</a>
                            </li>
                        </ul>
                    </div>
                    <div className="col-4">
                        <ul className="list-inline list-inline-dots mb-0">
                            <li className="list-inline-item">
                                Copyright © 2022
                                <a href="." className="link-secondary"> BauxiteDEV</a>.
                                All rights reserved.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    )

}