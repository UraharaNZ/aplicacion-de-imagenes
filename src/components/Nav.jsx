import logo from '../logo.png'

export const Nav = () => {

    return (
        <div>
            <header className="navbar navbar-expand-md navbar-light d-print-none">
                <div className="container-xl">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbar-menu">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <h1 className="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
                        <a href="https://bauxite.dev">
                            <img src={logo} width="110" height="32" alt="BauxiteDEV"
                                 className="navbar-brand-image"/>
                        </a>
                    </h1>
                    <div className="navbar-nav flex-row order-md-last">
                        <div className="nav-item d-none d-md-flex">
                            <div className="btn-list">
                                <a href="https://gitlab.com/UraharaNZ" className="btn" target="_blank"
                                   rel="noreferrer">
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         className="icon icon-tabler icon-tabler-brand-gitlab" width="24" height="24"
                                         viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none"
                                         strokeLinecap="round" strokeLinejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z"></path>
                                    </svg>
                                    Gitlab
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div className="navbar-expand-md">
                <div className="collapse navbar-collapse" id="navbar-menu">
                    <div className="navbar navbar-light">
                        <div className="container-xl">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="">
                                        <span className="nav-link-icon d-md-none d-lg-inline-block">
                                          <svg xmlns="http://www.w3.org/2000/svg" className="icon" width="24" height="24"
                                               viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round"
                                               strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><polyline
                                              points="5 12 3 12 12 3 21 12 19 12"/><path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7"/><path
                                              d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6"/></svg>
                                        </span>
                                        <span className="nav-link-title">Home</span>
                                    </a>
                                </li>
                                <li className="nav-item dropdown active">
                                    <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false">
                                        <span className="nav-link-icon d-md-none d-lg-inline-block">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="icon" width="24" height="24"
                                                 viewBox="0 0 24 24" strokeWidth="2" stroke="currentColor" fill="none"
                                                 strokeLinecap="round" strokeLinejoin="round"><path stroke="none" d="M0 0h24v24H0z"
                                                                                                    fill="none"/><polyline
                                                points="12 3 20 7.5 20 16.5 12 21 4 16.5 4 7.5 12 3"/><line x1="12" y1="12" x2="20"
                                                                                                            y2="7.5"/><line x1="12" y1="12"
                                                                                                                            x2="12"
                                                                                                                            y2="21"/><line
                                                x1="12" y1="12" x2="4" y2="7.5"/><line x1="16" y1="5.25" x2="8" y2="9.75"/></svg>
                                        </span>
                                        <span className="nav-link-title">Projects</span>
                                    </a>
                                    <div className="dropdown-menu">
                                        <div className="dropdown-menu-columns">
                                            <div className="dropdown-menu-column">
                                                <a className="dropdown-item active" href="./images">
                                                    Gifts
                                                    <span className="badge badge-sm bg-green text-uppercase ms-2">New</span>
                                                    <span className="badge badge-sm bg-azure text-uppercase ms-2">REACT</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="dropdown-menu-column">
                                            <a className="dropdown-item" href="https://www.spigotmc.org/resources/dynamicbungeeauth-premium-command-semi-premium-system-sessions.27480/">
                                                DynamicBungeeAuth
                                                <span className="badge badge-sm bg-bitbucket text-uppercase ms-2">Java</span>
                                                <span className="badge badge-sm bg-lime text-uppercase ms-2">Minecraft</span>
                                            </a>
                                        </div>
                                        <div className="dropdown-menu-column">
                                            <a className="dropdown-item" href="">
                                                SpringAPI
                                                <span className="badge badge-sm bg-bitbucket text-uppercase ms-2">Java</span>
                                                <span className="badge badge-sm bg-gradient text-uppercase ms-2">SpringBoot</span>
                                                <span className="badge badge-sm bg-red text-uppercase ms-2">Ejemplo</span>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

}