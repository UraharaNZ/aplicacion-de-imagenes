import {getGifts} from "../../src/helpers/getGifts";

describe('Pruebas en getGifts', function () {

    test('Debe de retornar un arreglo de gifts', async () => {
        const gifts = await getGifts('Goku');

        expect(gifts.length).toBeGreaterThan(0);

        expect(gifts[0]).toEqual({
            id: expect.any(String),
            title: expect.any(String),
            url: expect.any(String),
            username: expect.any(String)
        })
    });

});