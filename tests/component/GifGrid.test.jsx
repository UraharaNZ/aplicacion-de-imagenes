import {fireEvent, render, screen} from "@testing-library/react";
import {GifGrid} from "../../src/components";
import {useFetchGifts} from "../../src/hooks/useFetchGifts";

jest.mock('../../src/hooks/useFetchGifts');

describe('Pruebas en GifGrid', function () {

    const search = {
        id: 'asd',
        category: 'Goku',
    }

    test('Debe de cambiar el valor de la caja de texto', () => {

        useFetchGifts.mockReturnValue({
            images: [],
            isLoading: true
        });

        render(<GifGrid search={search}/>);
        expect(screen.getByText('Loading...'));
        expect(screen.getByText(search.category));
    });

    test('Debe de mostrar items cuando se cargan las imagenes usando useFetchGifts ', () => {

        const gifs = [{
                id: 'ABC',
                title: 'Goku',
                url: 'https://www.google.cl/goku',
                username: 'Goku'
            },
            {
                id: 'CBA',
                title: 'Dragon Ball',
                url: 'https://www.google.cl/dragon_ball',
                username: 'Goku'
            }]

        useFetchGifts.mockReturnValue({
            images: gifs,
            isLoading: false
        });

        render(<GifGrid search={search}/>);
        expect(screen.getAllByRole('img').length).toBe(2);
    });

});