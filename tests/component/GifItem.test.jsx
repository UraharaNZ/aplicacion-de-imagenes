import {GifItem} from "../../src/components";
import {render, screen} from "@testing-library/react";

describe('Pruebas en GiftItem', function () {

    const title = 'Goku';
    const url = 'https://goku.net/image.png';
    const username = 'Goku';

    test('Debe de hacer match con el Snapshot', () => {
        const {container} = render(<GifItem title={title} url={url} username={username} />)
        expect(container).toMatchSnapshot();
    });

    test('Debe de mostrar la imagen con el URL y el alt indicado', () => {
        render(<GifItem title={title} url={url} username={username} />)

        const {src, alt} = screen.getByRole('img');

        expect(src).toBe(url);
        expect(alt).toBe(title);
    });

});