import {useFetchGifts} from "../../src/hooks/useFetchGifts";
import {renderHook, waitFor} from "@testing-library/react";

describe('Pruebas en el hook useFetchGifts', function () {

    test('Debe de regresar el estado inicial', () => {

        const {result} = renderHook(() => useFetchGifts('Goku'));
        const {images, isLoading} = result.current;

        expect(images.length).toBe(0);
        expect(isLoading).toBeTruthy();

    });

    test('Debe de retornar un arreglo de imagenes y isLoading en false', async () => {

        const {result} = renderHook(() => useFetchGifts('Goku'));
        await waitFor(
            () => expect(result.current.images.length).toBeGreaterThan(0)
        );

        const {images, isLoading} = result.current;

        expect(images.length).toBeGreaterThan(0);
        expect(isLoading).toBeFalsy();
    });

});